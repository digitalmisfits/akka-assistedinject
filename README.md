# akka-assistedinject
Akka + Guice w/ AssistedInject depency injection

---

Enables AssistedInject on Guice enabled actor.

## Rationale

When using constructor injection in actors one generally looses the possibility of bootstrapping the actor with regular constructor arguments (such as passing immutable Actor id's for example)
and one is generally left with bootstrapping the actor by sending an additional bootstrap message to the actor initialized with a DI framework (such as Guice).

By leveraging Guice AssistedInject it's possible to have injected construtor arguments (beans) AND pass-in additional parameters specific to the prototype'd scoped actor
when instantiating an actor through the ActorSystem actorOf facility. 

For now, the GuiceActorProducer is only capable of injecting a predetermined number of assisted constructor arguments. Technically Guice can inject a variable number of
assisted constructor arguments, however, Akka does not support varargs arguments for the IndirectActorProducer.

However, if you would wrap all the assisted constructor arguments into a contrainer (e.a. in the example that is TimeActorConfig<? extends ActorConfig)), the result will be the same.

### Example


Guice configuration:

```java
public class AkkaModule extends AbstractModule {

    @Override
    protected void configure() {

        binder().requireExplicitBindings();

        install(new FactoryModuleBuilder()
                .implement(TimeActor.class, TimeActor.class)
                .build(ActorFactory.class));

        bind(Clock.class).to(RealClock.class);
    }
}
```


ActorFactory implementation:

```java

@SuppressWarnings("unused")
public interface ActorFactory {

    TimeActor createTimeActor(TimeActorConfig config);
}
```


Actor implemenation:

```java

	public class TimeActor extends AbstractActor {

    private final Clock clock;
    private final TimeActorConfig config;

    @Inject
    public TimeActor(Clock clock, @Assisted TimeActorConfig config) {
        this.clock = clock; // <-- injected
        this.config = config; // <-- user passed
    }

```


Creating a new actor with (a) custom (assisted) parameter(s):

```java
	final ActorRef timeActorRef = system.actorOf(Props.create(GuiceActorProducer.class, TimeActor.class, new TimeActorConfig("Africa/Windhoek")));
```

